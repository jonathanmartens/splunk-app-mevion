# Mevion app for Splunk
This is a plugin for [Splunk](https://www.splunk.com/) to parse logfiles created by [Mevion Medical Systems](http://www.mevion.com/) Protontherapy equipment.

# Getting started

## Pre-requisites
To run this you need to have the following installed: 
- [Docker](https://www.docker.com/) 
- [Docker Compose](https://docs.docker.com/compose/install/)

## Deployment
1. Make sure to set the initial password in the .env file.
2. Run ```docker-compose up -d```
3. After start-up of the container the application should be accessible via a browser at http://localhost:8000/

## Usage
1. To seperate the logs of different machines create a subfolder for every machine in the logs folder. The name of this folder will be used as hostname in Splunk searches and reports
2. Place your TCLoggerl.csv logs inside the directory to have them indexed. All logfiles placed in these subfolder ending in .csv will be processed.

## Authors
Jonathan Martens - Initial work - Jonathan Martens <jonathan.martens@maastro.nl>